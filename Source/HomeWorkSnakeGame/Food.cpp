// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/SphereComponent.h"



// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<USphereComponent>("RootFood");
	RootComponent = MyRootComponent;

	SnakeFoodMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	UMaterialInstance* FoodColor;
	FoodColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/Materials/InstanceMaterial/M_Metal_Gold_Inst.M_Metal_Gold_Inst'")).Get();

	FVector Size = FVector(0.5f, 0.5f, 0.5f);

	UStaticMeshComponent* FoodChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Food"));

	FoodChank->SetStaticMesh(SnakeFoodMesh);
	FoodChank->SetRelativeScale3D(Size);
	FoodChank->SetRelativeLocation(FVector(0, 0, 0));
	FoodChank->SetMaterial(0, FoodColor);
	FoodChank->AttachTo(MyRootComponent);
	FoodChank->SetSimulatePhysics(false);
}


// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->Score++;
			Destroy(true, true);
		}
	}
}

