// Copyright Epic Games, Inc. All Rights Reserved.

#include "HomeWorkSnakeGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HomeWorkSnakeGame, "HomeWorkSnakeGame" );
