// Fill out your copyright notice in the Description page of Project Settings.


#include "Death.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Engine/Classes/Components/BoxComponent.h"

// Sets default values
ADeath::ADeath()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;
	WallColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/Materials/InstanceMaterial/M_CobbleStone_Pebble_Inst.M_CobbleStone_Pebble_Inst'")).Get();

	MyRootComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("RootModel"));
	RootComponent = MyRootComponent;
	MyRootComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	UStaticMeshComponent* WallChank;
	WallChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	WallChank->SetStaticMesh(WallMesh);
	WallChank->SetRelativeLocation(FVector(0, 0, 0));
	WallChank->SetMaterial(0, WallColor);
	WallChank->AttachTo(MyRootComponent);
}

// Called when the game starts or when spawned
void ADeath::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADeath::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADeath::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeElementBase>(Interactor);
		Snake->Destroy(true, true);
	
	}
}





