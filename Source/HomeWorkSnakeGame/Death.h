// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Death.generated.h"


class UBoxComponent;


UCLASS()
class HOMEWORKSNAKEGAME_API ADeath : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADeath();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UBoxComponent* MyRootComponent;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UMaterialInstance* WallColor;

	virtual void Interact(AActor* Interactor, bool bIsHead);
	
};
