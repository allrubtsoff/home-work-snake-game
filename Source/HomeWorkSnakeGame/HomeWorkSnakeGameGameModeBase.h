// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HomeWorkSnakeGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORKSNAKEGAME_API AHomeWorkSnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
